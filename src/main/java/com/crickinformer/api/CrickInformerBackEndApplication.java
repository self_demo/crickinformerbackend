package com.crickinformer.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrickInformerBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrickInformerBackEndApplication.class, args);
	}

}
