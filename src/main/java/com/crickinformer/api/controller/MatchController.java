package com.crickinformer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crickinformer.api.entities.Match;
import com.crickinformer.api.service.MatchService;

@RestController
@RequestMapping("/match")
@CrossOrigin("*")
public class MatchController {

	@Autowired
	private MatchService matchService;

	@GetMapping("/live")
	public ResponseEntity<List<Match>> getLiveMatches() {
		return new ResponseEntity<>(matchService.getLiveMatches(), HttpStatus.OK);

	}

	@GetMapping
	public ResponseEntity<List<Match>> getAllMatches() {
		return new ResponseEntity<>(matchService.getAllMatches(), HttpStatus.OK);

	}

	@GetMapping("/point-table")
	public ResponseEntity<?> getPointTable() {
		return new ResponseEntity<>(matchService.getPointTable(), HttpStatus.OK);

	}
}
